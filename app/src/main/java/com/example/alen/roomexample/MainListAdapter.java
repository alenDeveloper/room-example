package com.example.alen.roomexample;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alen.roomexample.database.User;

import java.util.ArrayList;
import java.util.List;

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.ViewHolder> {

    List<User> users = new ArrayList<>();
    OnUserClickListener listener;

    public MainListAdapter(List<User> users, OnUserClickListener listener) {
        this.users = users;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_list_row, parent, false);

        return new ViewHolder(itemView, listener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = users.get(position);

        holder.name.setText(user.getUserName());
        holder.lastname.setText(user.getUserLastName());
        holder.email.setText(user.getUserEmail());
        holder.user = user;
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,lastname,email;
        ImageView info;
        User user;
        public ViewHolder(View itemView, final OnUserClickListener listener) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            lastname = itemView.findViewById(R.id.last_name);
            email = itemView.findViewById(R.id.email);
            info = itemView.findViewById(R.id.info);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View view) {
                    listener.onUserClick(user);
                    return false;
                }
            });
            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onUserTap(user.getId());
                }
            });
        }
    }

    public interface OnUserClickListener {
        void onUserClick(User user);
        void onUserTap(int id);
    }
}
