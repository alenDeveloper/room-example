package com.example.alen.roomexample.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM User")
    List<User> getAllUser();

    @Insert
    void insertAll(User... users);

    @Delete
    int deleteUser(User user);

    @Query("SELECT * FROM User WHERE id = :id")
    User getUserById(int id);
}
