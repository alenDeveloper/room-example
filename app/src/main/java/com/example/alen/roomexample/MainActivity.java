package com.example.alen.roomexample;

import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.alen.roomexample.database.AppDatabase;
import com.example.alen.roomexample.database.User;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    FloatingActionButton fab;
    RecyclerView mainList;
    AppDatabase db;
    private static final String TAG = "MainActivity";

    MainListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fab = findViewById(R.id.fab);
        mainList = findViewById(R.id.main_list);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "onClick:" + " fab button clicked");
                startActivity(new Intent(MainActivity.this, CreateUserActivity.class));
            }
        });

        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "production")
                .allowMainThreadQueries()
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUserList();
    }

    private void setUserList() {
        List<User> users = db.userDao().getAllUser();

        setAdapter(users);
    }

    private void setAdapter(final List<User> users) {

        MainListAdapter.OnUserClickListener listener = new MainListAdapter.OnUserClickListener() {
            @Override
            public void onUserClick(User user) {
                db.userDao().deleteUser(user);

                int position = users.indexOf(user);
                users.remove(user);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, users.size());
            }

            @Override
            public void onUserTap(int id) {
                User user = db.userDao().getUserById(id);

                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("User");
                alert.setMessage(user.getUserName() + " " + user.getUserLastName() + "\n" + user.getUserEmail());
                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alert.show();
            }
        };

        adapter = new MainListAdapter(users, listener);
        mainList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mainList.setAdapter(adapter);
    }

}
