package com.example.alen.roomexample;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.alen.roomexample.database.AppDatabase;
import com.example.alen.roomexample.database.User;

public class CreateUserActivity extends AppCompatActivity {

    EditText firstName;
    EditText lastName;
    EditText email;
    Button saveUser;
    AppDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);

        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "production")
                .allowMainThreadQueries()
                .build();

        firstName = findViewById(R.id.first_name_edit);
        lastName = findViewById(R.id.last_name_edit);
        email = findViewById(R.id.email_edit);
        saveUser = findViewById(R.id.save_user_btn);

        saveUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveToDatabase();
                onBackPressed();
            }
        });
    }

    private void saveToDatabase() {
        String name = firstName.getText().toString();
        String last = lastName.getText().toString();
        String mail = email.getText().toString();

        User user = new User(name, last, mail);
        db.userDao().insertAll(user);
    }
}
